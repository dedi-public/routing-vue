import Home from './components/Home.vue';
import NotFound from './components/NotFound.vue';
import User from './components/user/User.vue';
import UserStart from './components/user/UserStart.vue';
import UserEdit from './components/user/UserEdit.vue';
import UserDetail from './components/user/UserDetail.vue';

export const routes = [
	{ path: '', component: Home, name:'home'},
	{ path: '/user', component: User, children: [
		{ path: '', component: UserStart },
		{ path: ':id', component:UserDetail },
		{ path: ':id/edit', component:UserEdit, name: 'userEdit' }
	]},
	{ path: '/notfound', component: NotFound, name: "notfound" },
	{ path: '/redirect-me', redirect:{ name : 'home'} },
	{ path: '*', redirect:{ name: 'notfound'} }
];